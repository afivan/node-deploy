# Trigger a deployment from parent CI task

Auto deployment trigger in Gitlab CI. You can follow my guide [here](https://afivan.com/2024/03/08/gitlab-trigger-pipeline-in-another-repository-for-easy-deployment/)

## Getting started

In the .gitlab-deploy.yml you can find sample code which can be adapted to your use-case right away!

## Installation
No installation of special modules is necessary. You can integrate the necessary files into your project very easily.

## Usage
This project's pipeline is not intended to be run directly. It is triggered by [this](https://gitlab.com/afivan/node-autoincrement) parent project giving an DevOps team the separation they need for infrastructure scripts, processes, configurations and so forth.

## Support
For any questions, feedback or help please [contact me](https://afivan.com/contact/)

## Roadmap
There might be some new additions, stay tuned!

## Contributing
Any idea or contribution is more than welcomed!

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
MIT license, everyone is free to share and contribute

## Project status
It will be more or less updated...
